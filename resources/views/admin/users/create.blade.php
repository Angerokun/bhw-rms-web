@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div align='right' style="margin-bottom: 20px;">
                <a href="{{ route('users.index') }}"><button type="button" class="btn btn-primary"><i class="fas fa-chevron-left"></i> Back</button></a>
            </div>
            <div class="card">
                <div class="card-header">Create New User</div>

                <div class="card-body">
                    <form method="POST" id ="user-create" action="{{ route('users.store') }}">
                        @csrf
                        <h4><b>Add User Information</b></h4>
                        <div class="errors"></div>
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">Product Name</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="product_name" value="" data-validation="required">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">Product Description</label>
                            <div class="col-md-6">
                                <textarea class="form-control" name="product_desc" ></textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">Category</label>
                            <div class="col-md-6">
                                <select class="form-control" name="category" id="category" data-validation="required">
                                    <option value="">Select Category</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">Sub Category</label>
                            <div class="col-md-6">
                                <select class="form-control" name="sub_category" id="sub_category">
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">Stock Status</label>
                            <div class="col-md-6">
                                <select class="form-control" name="product_status">
                                    <option value="1">In Stock</option>
                                    <option value="4">Out of Stock</option>.
                                    <option value="2">Pre Order</option>
                                    <option value="3">2-3 Days</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">Status</label>
                            <div class="col-md-6">
                                <select class="form-control" name="status">
                                    <option value="1">Enabled</option>
                                    <option value="0">Disabled</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">Product Quantity</label>
                            <div class="col-md-6">
                            <input type="text" class="form-control qty" name="product_qty" value="" data-validation="required">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">Product Price</label>
                            <div class="col-md-6">
                            <input type="text" class="form-control money" name="product_price" value="" data-validation="required">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">Sort Order</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="sort_order" data-validation="required">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">Add this product to featured products?</label>
                            <div class="col-sm-6">
                            <label class="switch">
                                <input type="checkbox" id="featured_product" name="featured_product" >
                                <div class="slider"></div>
                            </label>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">Add this product to latest products?</label>
                            <div class="col-sm-6">
                            <label class="switch">
                                <input type="checkbox" id="latest_product" name="latest_product" >
                                <div class="slider"></div>
                            </label>
                            </div>
                        </div>
                        <div align="right">
                            <button type="submit" id="submit_product" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                    <div id="result">
                        <code id="result-data"></code>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@push('scripts')
<script>
    $('#user-create').register_fields('.errors');
</script>
@endpush
@endsection
