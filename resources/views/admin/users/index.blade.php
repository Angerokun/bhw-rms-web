@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div align='right' style="margin-bottom: 20px;">
                <a href="{{ route('users.create') }}"><button type="button" class="btn btn-primary"><i class="fas fa-plus"></i> New</button></a>
            </div>
            <div class="card">
                <div class="card-header"><i class="fas fa-list"></i> Users</div>

                <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-sm table-bordered table-striped" id="product-table">
                        <thead class="thead-dark">
                            <tr>
                                <th scope="col" width="8%">Image</th>
                                <th scope="col" width="15%">Product Name</th>
                                <th scope="col" width="15%">Price</th>
                                <th scope="col" width="15%">Quantity</th>
                                <th scope="col" width="15%">Status</th>
                                <th scope="col" width="8%">Action</th>
                            </tr>
                        </thead>
                    </table>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection